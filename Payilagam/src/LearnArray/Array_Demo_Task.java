package LearnArray;          //   08th oct 2023

public class Array_Demo_Task {
	
	public static void main(String[] args) {
		
	int marks[]= {376,294,491,169,413};	
	
	String student[]= {"Arun","Bala","Chandru","Dinesh","Fayaz"};
		
		
	Array_Demo_Task.marksChecking(marks,student);	
		
	}

	private static void marksChecking(int marks[],String student[]) {      // TASK 1 NAME & TOTAL &  GRADE 

		for(int i=0;i<student.length;i++) {
			
			System.out.print("NAME : "+student[i]+" ");
			
			for(int j=0;j<marks.length;j++) {
				
				if(i==j) {
					System.out.print("  ||  TOTAL : "+marks[j]);
					
					if(marks[j]>490 && marks[j]<500) {
						System.out.print("  ||  A+  GRADE");
					}
					else if(marks[j]>480 && marks[j]<490) {
						System.out.print("  ||   A GRADE");
					}
					else if(marks[j]>465 && marks[j]<480) {
						System.out.print("  ||   B GRADE");
					}
					else if(marks[j]>400 && marks[j]<465) {
						System.out.print("  ||   C GRADE");
					}
					else if(marks[j]>350 && marks[j]<400) {
						System.out.print("  ||   D GRADE ");
					}else if(marks[j]>300 && marks[j]<350) {
						System.out.print("  ||   E GRADE");
					}else {
						System.out.print("  ||   RA FAIL ");
					}
				}
			}
			
			System.out.println();
			
			System.out.println();
     }
  }
	}


