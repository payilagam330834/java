package LearnArray;

public class Task1_Multidimensional {
	public static void main(String[] args) {
		
		
	int arr[][]= {{1,2,3},{4,5,6},{7,8,9}};	
		
	for(int i=0;i<arr.length;i++) {     //Task 1 --> 3 X 3 matrix
		for(int j=0;j<arr[i].length;j++) {
			System.out.print(arr[i][j]+" ");  //printing array elements
		}
		System.out.println();
	}
	System.out.println("Cross Dimensional Array Elements");
	for(int i=0;i<arr.length;i++) {     //Task 2 --> 3 X 3 matrix
		for(int j=0;j<arr[i].length;j++) {
			if(arr[i]==arr[j])
			System.out.print(arr[i][j]+" ");  //printing  Cross dimensional array elements
		}
		System.out.println();
	}
  }
}

